subroutine propagation(psi0)

    use data_grid
    use pot_param
    use data_au
    use omp_lib
    use, intrinsic :: iso_c_binding

    implicit none

    include 'fftw3.f03'

    integer I, J, K

    type(C_PTR):: planB, planF
    integer*8:: void
    double precision:: time, tts, tte, step_time    ! nun - die Zeit
    double precision:: R      ! Kernabstand
    double precision:: E      ! Energie (E_kin der Photoelektronen)
    double precision:: cpm      ! Abschneideparameter
    double precision:: norm    ! die Gesamtnorm der Wellenfunktion
    double precision:: norm_ionic    ! die Norm im ionischen Zustand
    double precision:: evR      ! Ortserwartungswert
    double precision:: cof(nr)    ! Abschneidefunktion
    double precision, intent(in):: psi0(nr) ! die Grundzustandswellenfunktion

    complex*16:: laser1      ! Das Feld des Anregungslasers

    double precision, allocatable, dimension(:):: pes  ! Photoelektronenspektrum
    complex*16, allocatable, dimension(:):: laser2    ! Das Feld des ionisierenden Lasers
    complex*16, allocatable, dimension(:):: psi    ! Wellenfunktion, angeregter Zustand
    complex*16, allocatable, dimension(:):: kprop    ! Kinetischer Propagator
    complex*16, allocatable, dimension(:):: vprop    ! Potentieller Propagator
    complex*16, allocatable, dimension(:, :):: psi_ionic  ! Ionische Wellenfunktion
    complex*16, allocatable, dimension(:):: psi_e    ! Zwischenfunktion psi_ionic

    open (100, file='out/psi0.dat', status='unknown')     ! Anfangswellenfunktion - 2D
    open (101, file='out/cut_off_function.dat', status='unknown') ! Die Abschneidefunktion - 2D
    open (102, file='out/differenz_pot.dat', status='unknown')  ! Differenzpotential V_ionic(R) - V_excited(R)
    open (200, file='out/dens.dat', status='unknown')    ! Kernwellenpaket im unteren Zustand - 3D
    open (202, file='out/psi_i_end.dat', status='unknown')  ! Dichte im ionischen Zustand am Ende der Prop.
    open (300, file='out/laser1.dat', status='unknown')    ! Laserfeld des anregenden Lasers - 2D
    open (301, file='out/laser2.dat', status='unknown')    ! Laserfeld des ionisierenden Lasers - 2D
    open (800, file='out/R.dat', status='unknown')    ! Ortserwartungswert - 2D, eine Spalte
    open (908, file='out/norm.dat', status='unknown')    ! Norm im el. Zustand- 2D, eine Spalte
    open (909, file='out/norm_ionic.dat', status='unknown')  ! Norm im ionischen Zustaenden- 2D, eine Spalte
    open (900, file='out/spectrum.dat', status='unknown')    ! Besetzung der Vibrationszustaende - 2D

    allocate (psi(NR), kprop(NR), vprop(NR), psi_e(nr), pes(energy_points))
    allocate (laser2(energy_points), psi_ionic(nr, energy_points))

    call dfftw_plan_dft_1d(planF, NR, psi, psi, FFTW_FORWARD, FFTW_MEASURE)
    call dfftw_plan_dft_1d(planB, NR, psi, psi, FFTW_BACKWARD, FFTW_MEASURE)


    psi = (0.d0, 0.d0)        ! Initialisierung
    psi_ionic = (0.d0, 0.d0)

    cpm = 1.5d0/au2a

    do j = 1, NR      ! Das ist eine Abschneidefunktion, die verhindern soll,
        R = R0 + (j - 1)*dR  ! dass ein Wellenpaket, wenn es dissoziiert, an das Ende
        if (R .lt. (Rend - cpm)) then  ! des Grids stoesst.
            cof(j) = 1.d0    ! Daher schneidet diese Funktion (cut-off-function) die
        else      ! auslaufende Wellenfunktion sanft ein paar Angstrom
            cof(j) = cos(((R - Rend + cpm)/-cpm)*(0.5d0*pi))  ! (definiert durch den
            cof(j) = cof(j)**2    ! Parameter CPM) vorher ab.
        end if
        write (101, *) sngl(R*au2a), sngl(cof(j))
    end do

    !$OMP PARALLEL DO
    do i = 1, NR
        kprop(i) = exp(-im*dt*Pr(i)**2/(2.d0*mass)) ! Kinetischer Propagator
        vprop(i) = exp(-0.5d0*im*dt*pot(i, 2))    ! Potentieller Propagator - angeregter
    end do             ! Zustand (pot(R,2))
    !$OMP END PARALLEL DO

    do I = 1, NR
        R = R0 + (I - 1)*dR
        write (100, *) sngl(R*au2a), sngl(abs(psi0(I))**2)
        write (102, *) sngl(R*au2a), sngl((pot(i, 3) - pot(i, 2))*au2eV)
    end do

!______________________________________________________________________
!
!                   Propagation Loop
!_____________________________________________________________________

    print *
    print *, '1D propagation...'
    print *

    ! call cpu_time(tts)
    tts = omp_get_wtime()

    timeloop: do K = 1, Nt      ! Zeitschleife

        step_time = omp_get_wtime()

        time = K*dt

!============== Definition der Laserfelder ====================================

        Laser1 = exp(-fwhm1*(time - t_1)**2)*exp(-im*omega1*time)

        !$OMP PARALLEL DO
        do j = 1, energy_points   ! Energieerhaltung - die Energien der Photoelektronen
            E = E0 + (j - 1)*dE   ! entsprechen E = omega_2 - Delta V(R)
            laser2(j) = exp(-fwhm2*(time - t_2)**2 - im*(omega2 - E)*time)
        end do
        !$OMP END PARALLEL DO

! ============= Propagationsteil ==============================================
!
! Die Propagation findet dieses Mal fuer die Wellenfunktionen in beiden
! elektronischen Zustaenden, im angeregten und im ionischen Zustand statt.
! _____________________________________________________________________________

        ! Anregung in den Doppelminimumzustand via 1. Ordnung zeitabh. Stoerungstheorie
        psi(:) = psi(:) + dt * psi0(:) * laser1

        !$OMP PARALLEL DO
        do J = 1, energy_points   ! Ionisation auch via Stoerungstheorie (formal 2. Ordnung)
            psi_ionic(:, j) = psi_ionic(:, j) + dt*psi(:)*laser2(j)
        end do
        !$OMP END PARALLEL DO

        ! ......Potentielle Propagation ........................

        psi = psi * vprop        ! Angeregter Zustand

        !$OMP PARALLEL DO
        do i = 1, energy_points      ! Ionischer Zustand
            psi_ionic(:, i) = psi_ionic(:, i)*exp(-0.5d0*im*dt*pot(:, 3))
        end do
        !$OMP END PARALLEL DO

        ! ......Kinetische Propagation ....................

        call fftw_execute_dft(planF, psi, psi)      ! Fouriertransformation: Psi(R) --> Psi(P)
        psi = psi*kprop        ! Kinetische Propagation
        call fftw_execute_dft(planB, psi, psi)        ! Fouriertransformation: Psi(P) --> Psi(R)
        psi = psi/dble(NR)  ! Angeregter Zustand

        do i = 1, energy_points   ! Ionischer Zustand
            ! Jede Komponente (jede Energie) muss separat in den Impulsraum Fouriertransfomiert werden.
            ! psi_e ist eine Hilfsgroesse, in die zwischenkopiert wird.
            psi_e(:) = psi_ionic(:, i)

            call fftw_execute_dft(planF, psi_e, psi_e)     ! FT: Psi_ionic(R) --> Psi_ionic(P)
            psi_e = psi_e*kprop        ! Kinetische Propagation
            call fftw_execute_dft(planB, psi_e, psi_e)      ! FT: Psi_ionic(P) --> Psi_ionic(R)
            psi_e = psi_e/dble(nr)

            ! Zurueckkopieren der Hilfsgroesse psi_e in die urspruengliche Groesse psi_ionic
            psi_ionic(:, i) = psi_e(:)

        end do

        ! ......Potentielle Propagation ........................

        psi = psi*vprop        ! Angeregter Zustand

        !$OMP PARALLEL DO
        do i = 1, energy_points      ! Ionischer Zustand
            psi_ionic(:, i) = psi_ionic(:, i)*exp(-0.5d0*im*dt*pot(:, 3))
        end do
        !$OMP END PARALLEL DO

! ============ Ende der Propagation, hier kommt nur noch Output ==============

        call integ(psi, norm)      ! Berechnet die Norm im angeregten Zustand
        call integ_ionic(psi_ionic, norm_ionic)  ! Berechnet die Norm im ionischen Zustand

        evR = 0.d0    ! Ortserwarungswert: <R> = int R* |psi(R)|^2 *dR / int |psi(R)|^2 *dR

        do I = 1, NR
            R = R0 + (I - 1)*dR
            evR = evR + abs(psi(I))**2*R ! Ortserwarungswert <R> = int |psi(R)|^2,  Numerische Integration
        end do

        evR = evR*dR        ! Ortserwartungswert  *dR

        evR = evR/norm               !  / int |psi(R)|^2 *dR

        write (800, *) sngl(time*au2fs), sngl(evR*au2a)  ! Schreibt den Ortserwartungswert raus
        write (908, *) sngl(time*au2fs), sngl(norm)      ! Schreibt die Population im angeregt. Zustand
        write (909, *) sngl(time*au2fs), sngl(norm_ionic)  ! Schreibt die Population im ionischen Zustand
        write (300, *) sngl(time*au2fs), sngl(real(laser1))  ! Schreibt das Laserfeld raus
        write (301, *) sngl(time*au2fs), sngl(real(laser2(75)))  ! Schreibt das Laserfeld raus

        if (mod(K, 100) .eq. 0) then        ! Nur jeden 200. Zeitpunkt, sonst Datei zu groß
            do I = 1, NR
                R = R0 + (I - 1)*dR
                if (mod(I, 4) .eq. 0) then  ! Nur jeder 4. Ortspunkt, sonst Datei zu groß
                    write (200, *) sngl(time*au2fs), sngl(R*au2a), sngl(abs(psi(I)**2))  ! Dichte |psi(R,t)|^2
                end if
            end do
            write (200, *)
        end if

        psi = psi*cof        ! Der rauslaufende Teil wird entfernt

        !$OMP PARALLEL DO
        do i = 1, energy_points      ! ebenso fuer den ionischen Zustand
            psi_ionic(:, i) = psi_ionic(:, i)*cof(:)
        end do
        !$OMP END PARALLEL DO


        if (mod(K, 100) .eq. 0) then    ! Schreibt alle 100 Werte die Zeit ins Terminal
            print *, 'Step:', K, 'Curr. time:', sngl(time*au2fs), "Step time:", sngl(omp_get_wtime() - step_time)
        end if


    end do timeloop          ! Ende der Zeitschleife

    ! call cpu_time(tte)
    tte = omp_get_wtime()

    write (*, *) "Time to finish: ", tte - tts

    do I = 1, 3*NR/4, 4
        R = R0 + (I - 1)*dR
        do j = 1, energy_points
            E = E0 + (j - 1)*dE
            write (202, *) sngl(R*au2a), sngl(E*au2eV), sngl(abs(psi_ionic(I, j)**2))  ! Dichte |psi(R,t)|^2
        end do
        write (202, *)
    end do
    call spectrum(psi_ionic, pes)

    do i = 1, energy_points
        E = E0 + (i - 1)*dE
        write (900, *) sngl(E*au2eV), sngl(pes(i))
    end do

    close (100, status='keep')
    close (101, status='keep')
    close (102, status='keep')
    close (200, status='keep')
    close (300, status='keep')
    close (301, status='keep')
    close (800, status='keep')
    close (908, status='keep')
    close (909, status='keep')
    close (900, status='keep')

    call fftw_destroy_plan(planF)
    call fftw_destroy_plan(planB)

    deallocate (psi, kprop, vprop, psi_ionic, pes, psi_e)

    return
end subroutine

!_________________________________________________________

subroutine integ(psi, norm)

    use data_grid

    implicit none
    integer:: I
    double precision, intent(out):: norm
    complex*16, intent(in):: psi(NR)

! Dieses Unterprogramm rechnet die Norm der komplexen Wellenfunktion aus.

    norm = 0.d0

    !do I = 1, NR
        norm = sum(abs(psi(:))**2)
    !end do

    norm = norm*dR

    return
end subroutine

!_________________________________________________________

subroutine integ_ionic(psi_ionic, norm_ionic)

    use data_grid

    implicit none
    integer:: i, j
    double precision, intent(out):: norm_ionic
    complex*16, intent(in):: psi_ionic(NR, energy_points)

! Dieses Unterprogramm rechnet die Norm der ionischen Wellenfunktion aus.

    norm_ionic = 0.d0

    do j = 1, energy_points
        !do I = 1, NR
            norm_ionic = norm_ionic + sum(abs(psi_ionic(:, j))**2)
        !end do
    end do

    norm_ionic = norm_ionic*dR*dE

    return
end subroutine
!______________________________________________________________

subroutine spectrum(psi_ionic, pes)

    use data_grid

    implicit none
    integer:: i, j
    double precision, intent(out):: pes(energy_points)
    complex*16, intent(in):: psi_ionic(NR, energy_points)

! Dieses Unterprogramm rechnet das Photoelektronenspektrum aus.

    do j = 1, energy_points
        do i = 1, nr
            pes(j) = pes(j) + abs(psi_ionic(i, j))**2
        end do
        pes(j) = pes(j)*dR
    end do

    return
end subroutine

