module data_grid
    integer:: Nt                                         ! Anzahl der Zeitschritte
    integer, parameter:: Nr = 1024                         ! Anzahl der Punkte des Ortsgrids
    integer, parameter:: energy_points = 150                 ! Anzahl der Punkte des Energiegrids
    double precision:: dR, dpR, dt                         ! Schrittgroesse Orts-, Impuls- und Zeitgrid
    double precision:: dE                                 ! Schrittgroesse Energiegrid (f. Photoelektronen)
    double precision:: PR(nr)                          ! Impulsgrid
    double precision:: Pot(nr, 3)                         ! Die Potentialkurven
    double precision:: mass                         ! Die Masse des Systems
    double precision:: fwhm1, fwhm2                 ! Laenge der beiden Pulse
    double precision:: omega1, omega2                 ! Frequenzen der beiden Pulse
    double precision:: t_1, t_2                     ! Startzeiten der beiden Pulse
end module

module data_au
    double precision, parameter:: au2a = 0.52917706d0  ! Umrechnungsfaktor Laenge in a.u. --> Angstrom
    double precision, parameter:: cm2au = 4.5554927d-6 ! Umrechnungsfaktor Energie von Wellenzahlen --> a.u.
    double precision, parameter:: au2fs = 0.024        ! Umrechnungsfaktor Zeit von a.u. --> Femtosekunden
    double precision, parameter:: j2eV = 6.242D18    ! Umrechnungsfaktor Energie von J --> eV
    double precision, parameter:: au2eV = 27.2116d0    ! Umrechnungsfaktor Energie von a.u. --> eV
    double precision, parameter:: i2au = 2.0997496D-9
    double precision, parameter:: pi = 3.141592653589793d0    ! einfach nur pi
    double precision, parameter:: d2au = 0.3934302014076827d0 ! Umrechnungsfaktor Dipolmoment von Debye --> a.u.
    double precision, parameter:: amu = 1822.888d0    ! Atomic mass unit (atomare Masseneinheit)
    double precision, parameter:: m_na2 = 22.989770d0     ! Masse eines Na atoms in atomaren Einheiten
    complex*16, parameter:: im = (0.d0, 1.d0)                  ! Das ist i, die imaginaere Zahl
end module

module pot_param
    use data_au
    double precision, parameter:: R0 = 2.40d0/au2a           ! Ortsgrid Parameter, Anfang..
    double precision, parameter:: Rend = 16.d0/au2a   ! ..und Ende
    double precision, parameter:: E0 = 0.00d0           ! Energiegrid Parameter, Anfang..
    double precision, parameter:: Eend = 3.d0/au2eV   !..und Ende
end module pot_param

program wavepacket

    use data_grid
    implicit none

    double precision, allocatable, dimension(:):: psi0

    print *
    print *
    print *, 'Initialization...'
    print *

    call system('mkdir -p out/')

    call input
    call p_grid

    allocate (psi0(nr))

    call potential
    call nuclear_wavefkt(psi0)
    call propagation(psi0)

    print *, 'Finished'
    deallocate (psi0)

end program

! _______________ Subroutines __________________________________________________

subroutine input

    use data_grid
    use pot_param
    implicit none

    double precision:: lambda1, lambda2
    double precision:: tp1, tp2
    double precision:: R_eq

    open (10, file='in/input', status='old')

! In diesem Unterprogramm werden Parameter eingelesen und in atomare Einheiten
! umgerechnet.
! Parameter: Laserparameter

    read (10, *) Nt                                    ! Nt = number of time steps.
    read (10, *) lambda1                    ! = Wellenlaenge des Anregungslasers in nm
    read (10, *) lambda2                        ! = Wellenlaenge des Abfragelasers in nm
    read (10, *) tp1, tp2                        ! = Laenge Laserpuls 1, Laenge Laser 2
    read (10, *) t_1, t_2                        ! = Start Laserpuls 1, Start Laser 2

    dR = (Rend - R0)/(NR - 1)                ! Schrittweite des Ortsgrids
    dE = (Eend - E0)/(energy_points - 1)        ! Schrittweite des Energiegrids

    mass = 0.5*m_na2*amu                        ! Reduzierte Masse des Systems

    dt = 0.1d0/au2fs                        ! Zeitschritt
    R_eq = 3.0824563745/au2a                    ! Gleichgewichtsabstand im Na2 Grundzustand

    tp1 = tp1/au2fs                         ! Laenge des Laserpulses (in fs)
    tp2 = tp2/au2fs                         ! Laenge Puls 2 (in fs)
    t_1 = t_1/au2fs                          ! Laserpuls zentriert um diesen Zeitpunkt (in fs)
    t_2 = t_2/au2fs                        !     analog Puls 2
    fwhm1 = (4.d0*dlog(2.d0))/tp1**2  ! Laenge Laserpuls - Umrechnung fuer einen Gausspuls
    fwhm2 = (4.d0*dlog(2.d0))/tp2**2         !     analog Puls 2
    omega1 = (1.d0/(lambda1*1.d-7))*cm2au! Wellenlaenge des Pulses in nm
    omega2 = (1.d0/(lambda2*1.d-7))*cm2au

    print *, '_________________________'
    print *
    print *, 'Parameters'
    print *, '_________________________'
    print *
    print *, 'dt = ', sngl(dt*au2fs), 'fs'
    print *, 'dR = ', sngl(dR*au2a), 'A'
    print *, 'dE = ', sngl(dE*au2eV), 'eV'
    print *
    print *, 'Na2 Masse:', sngl(mass/amu)
    print *, 'Na2 R_eq:', sngl(R_eq*au2a), 'A'
    print *
    Print *, 'LASER Parameter _________'
    print *
    print *, 'Anregungslaser:'
    print *, 'Wellenlaenge = ', sngl(lambda1), 'nm'
    print *, '             = ', sngl(omega1*au2eV), 'eV'
    print *, 'Pulslaenge = ', sngl(tp1*au2fs), 'fs'
    print *, 'Pulsstart = ', sngl(t_1*au2fs), 'fs'
    print *
    print *, 'Abfragelaser:'
    print *, 'Wellenlaenge = ', sngl(lambda2), 'nm'
    print *, '             = ', sngl(omega2*au2eV), 'eV'
    print *, 'Pulslaenge = ', sngl(tp2*au2fs), 'fs'
    print *, 'Pulsstart = ', sngl(t_2*au2fs), 'fs'
    print *
    print *, '__________________________'
    print *

    close (10)
end subroutine

!...................... Impulsgrid......................

subroutine p_grid

    use data_grid
    use data_au
    implicit none
    integer:: I

! In diesem Unterprogramm wird das Grid im Impulsraum definiert. Auf diesem Grid
! ist die Wellenfunktion im Impulsraum definiert - die Schrittgroesse dPR haengt
! von der Schrittgroesse im Ortsraum, dR, und der Anzahl der Gridpunkte NR zusammen.

    dPR = (2.d0*pi)/(dR*NR)

    do I = 1, NR
        if (I .le. (NR/2)) then
            PR(I) = (I - 1)*dPR
        else
            PR(I) = -(NR + 1 - I)*dpR
        end if
    end do

    return
end subroutine

!........................... Einlesen des Potentials..............

subroutine potential

    use data_grid
    use data_au
    use pot_param

    implicit none

    integer:: i
    double precision:: R, dummy

    open (10, file='in/Na2_X.inp', status='old')
    open (11, file='in/Na2_A.inp', status='old')
    open (12, file='in/Na2_+.inp', status='old')
    open (20, file='out/Potential.dat', status='unknown')

! Hier werden die Potentiale, auf dem wir die Kerndynamik berechnen wollen,
! initialisiert und alles in atomare Einheiten umgerechnet.

    do i = 1, nr
        read (10, *) dummy, pot(i, 1)
        read (11, *) dummy, pot(i, 2)
        read (12, *) dummy, pot(i, 3)
    end do

    pot = pot*cm2au

    do I = 1, NR
        R = R0 + (I - 1)*dR
        write (20, *) sngl(R*au2a), sngl(pot(i, :)*au2eV)
    end do

    close (10)
    close (11)
    close (12)
    close (20, status='keep')

    return
end subroutine

! ......................... Schwingungseigenfunktion ....................

subroutine nuclear_wavefkt(psi0)

    use data_grid
    use pot_param
    use data_au
    use, intrinsic :: iso_c_binding
    implicit none

    type(C_PTR):: planB, planF

    integer:: I, K, V, G
    integer:: istep
    integer, parameter:: vstates = 1

    double precision:: dt2          ! Zeitschrittgroesse der imaginaeren Zeitpropagation
    double precision:: E, E1        ! Energie und Zwischenenergie zwischen 2 Zeitschritten
    double precision:: thresh          ! Konvergenzkriterium
    double precision:: R            ! Kernabstand (auf dem Grid)
    double precision:: R_eq          ! Gleichgewichtsabstand
    complex*16:: norm

    complex*16, allocatable, dimension(:):: vprop, kprop        ! Zeitenwicklungsoperatoren
    complex*16, allocatable, dimension(:):: psi, psi1        ! Wellenfunktionen
    complex*16, allocatable, dimension(:, :):: psi_store     ! Wellenfunktion (Speicher)
    double precision, intent(out):: psi0(nr)

    include 'fftw3.f03'

    open (98, file='out/gaussR.dat', status='unknown')
    open (99, file='out/psi_vib.dat', status='unknown')
    open (100, file='out/Evib.dat', status='unknown')

    allocate (psi(NR), psi1(NR), vprop(NR))
    allocate (kprop(NR), psi_store(NR, Vstates))

    call dfftw_plan_dft_1d(planF, NR, psi, psi, FFTW_FORWARD, FFTW_MEASURE)
    call dfftw_plan_dft_1d(planB, NR, psi, psi, FFTW_BACKWARD, FFTW_MEASURE)
! In diesem Teilprogramm wirden die Eigenfunktionen der Kern-Schroedinger Gleichung
! berechnet, also die Schwingungsfunktionen. Meistens reicht allerdings nur der
! Schwingungsgrundzustand, der wie ein Gauss aussieht.

! Prinzipiell koennen mehrere Kernwellenfunktionen (die do-Schleife V-Loop) in
! mehreren elektronischen Zustaenden berechnet werden, allerdings werden wir
! erst mal nur den Grundzustand in einem elektronischen Zustand berechen.

    dt2 = dt
    thresh = 1.d-12         ! Konvergenzkriterium
    istep = 1d6                ! Anzahl der Schritte, bis Konvergenz erreicht sein muss.

    print *
    print *, 'Calculating nuclear eigenstate...'
    print *

    R_eq = 3.0824563745/au2a

    do i = 1, NR
        vprop(i) = exp(-0.5d0*dt*pot(i, 1))        ! Definition des potentiellen Propagators
        kprop(i) = exp((-dt*Pr(i)**2)/(2.0d0*mass)) ! Definition des kinetischen Propagators
    end do

    Vloop: do V = 1, Vstates         ! Berechnung der verschiedenen Vibrationszustaende

        do i = 1, NR                  ! Wahl der korrekten Symmetry der Anfangswellenfunktion
            R = R0 + (i - 1)*dR
            psi(i) = exp(-1.*(R - R_eq)**2) +&         ! Anfangsfunktion: Gauss
              &   (-1.d0)**(V - 1)*exp(-1.*(R + R_eq)**2)
            write (98, *) sngl(R*au2a), sngl(real(psi(i)))
        end do
        write (98, *)

        call overlap(psi, psi, norm)        ! Berechnung der Norm der Anfangswellenfunktion
        psi = psi/sqrt(abs(norm))                ! .. und normieren der Anfangswellenfunktion auf 1

        E = 0.D0                                ! Energie am Anfang auf 0 setzen

!.......... Imaginary Time Propagation ........................

        do K = 1, istep

            psi1 = psi         ! Wellenfunktion des Iterationschritts (N - 1)
            E1 = E                   ! Eigenwert des Iterationschritts (N - 1)

            if (V .gt. 1) then           ! Wird nur verwendet, wenn hoehere Vibrationszustaende
                do G = 1, (V - 1)        ! berechnet weden sollen - niedrig liegende werden aus-
                    ! projiziert
                    call overlap(psi_store(1:NR, G), psi, norm)  ! (Ueberlapp zu den bereits
                    ! berechneten Funktionen)
                    do i = 1, NR
                        psi(i) = psi(i) - norm*psi_store(i, G)           ! Abziehen des Ueberlapps
                    end do

                end do
            end if

            psi = psi*vprop                        ! Propagator, potentieller Teil
            call fftw_execute_dft(planF, psi, psi)           ! Fourier-transformation in Impulsraum:  Psi(R) --> Psi(P)
            psi = psi*kprop                  ! Propagator, kinetischer Teil
            call fftw_execute_dft(planB, psi, psi)                   ! Fourier-transformation in Ortsraum:  Psi(P) --> Psi(R)
            psi = psi/dble(Nr)                ! renormieren ... Eigenart der numerischen Fouriertransf.
            psi = psi*vprop                    ! Propagator, potentieller Teil

            call eigenvalue_init(psi, psi1, E, dt)                ! Berechnung der Eigenwerte
            call overlap(psi, psi, norm)                        ! Berechnung der Norm

            psi = psi/sqrt(abs(norm))                ! .. und renormieren der propagierten Wellenfunktion auf 1

            if (abs(E - E1) .le. thresh) then                        ! Check, ob Konvergenz erreicht ist
                goto 10
            end if

        end do

        print *, 'Iteration not converged!'
        print *, 'Program stopped!'
        print *
        print *, 'E =', E/cm2au
        print *, 'E1 =', E1/cm2au
        print *, 'thresh =', thresh/cm2au
        print *, 'step =', K

        stop

10      continue                                        ! Falls Konvergenz erreicht wurde, geht es hier weiter

        print *, 'Vibrational State', v, 'Energie:', sngl(E*au2eV)
        write (100, *) v, sngl(E*au2eV)

        do I = 1, NR
            R = R0 + (I - 1)*dR                        ! Die konvergierte Wellenfunktion wird als Referenz fuer
            psi_store(I, V) = psi(I)                     ! den naechsten Schleife (= naechst-hoehere Wellenfunktion)
            write (99, *) sngl(R*au2a), sngl(real(psi(I)))        ! gespeichert und rausgeschrieben.
        end do
        write (99, *)

    end do Vloop                                       ! Ende der Schleife ueber die Vibrationseigenzustaende

    do i = 1, nr
        psi0(i) = real(psi_store(i, 1))                        ! Der Grundzustand
    end do

    print *

    close (98, status='keep')
    close (99, status='keep')
    close (100, status='keep')

    deallocate (psi, psi1, vprop, psi_store, kprop)

    call fftw_destroy_plan(planF)
    call fftw_destroy_plan(planB)

    return
end

! ........................................................

subroutine eigenvalue_init(A, B, E, dt2)

    use data_grid
    implicit none
    double precision:: e1, e2
    double precision, intent(in):: dt2
    complex*16:: norm
    complex*16, intent(in):: A(nr), B(nr)
    double precision, intent(out):: E

! Berechnen der Eigenwerte der propagierten Wellenfunktion

    call overlap(B, B, norm)  ! Berechnet den Ueberlapp zweier Wellenfunktionen
    e1 = abs(norm)

    call overlap(A, A, norm)
    e2 = abs(norm)

    E = (-0.5d0/dt2)*log(e2/e1)

    return
end subroutine

!...............................................

subroutine overlap(A, B, norm)

    use data_grid
    implicit none
    integer I
    complex*16, intent(in):: A(Nr), B(Nr)
    complex*16, intent(out):: norm

! Das ist der Ueberlapp zweier Wellenfunktionen

    norm = (0.d0, 0.d0)

    do I = 1, Nr
        norm = norm + conjg(A(I))*B(I)
    end do

    norm = norm*dr

    return
end subroutine
