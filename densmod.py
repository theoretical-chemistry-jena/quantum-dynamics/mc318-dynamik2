#! /usr/bin/env python3

fname = "out/dens.dat"

with open(fname, "r") as f:
    data = f.read()

data = data.replace("\n\n", "\n\n\n")

with open("out/densmod.dat", "w") as f:
    f.write(data)


