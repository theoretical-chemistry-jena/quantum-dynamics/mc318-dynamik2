
let
  sources = import ./nix/sources.nix { };
  pkgs = import sources.nixpkgs { };

  pythonEnv = pkgs.python3.withPackages(ps: with ps; [
    numpy
    scipy
    matplotlib
  ]);

in
  with pkgs; mkShell {
    name = "MC3.1.8";
    buildInputs = [

      # Basic software
      which
      git

      # Scientific software
      gfortran
      gnuplot
      fftw
      fprettify

      # Misc
      imagemagick

    ];

    shellHook = ''
      export FFTW_DIR=${fftw.dev}
    '';
  }