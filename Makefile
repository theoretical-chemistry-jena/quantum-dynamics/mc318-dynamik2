FC  		?= gfortran
FFLAGS		= -O3 -w -fopenmp -march=native
LDFLAGS 	= -lm
FFTWFLGS	=  -I${FFTW_IDIR} -L${FFTW_LDIR} -lfftw3

FFTW_DIR    ?= /usr
FFTW_IDIR   ?= $(FFTW_DIR)/include
FFTW_LDIR   ?= $(FFTW_DIR)/lib64

default: 	dynamik2

dynamik2:   	main.o   propagation.o

		${FC} *.o ${FFLAGS} ${FFTWFLGS} ${LDFLAGS} -o dynamik2

main.o:	 src/main.f90
	${FC} $< ${FFLAGS} ${FFTWFLGS} -c -o $@

propagation.o:	src/propagation.f90
	${FC} $< ${FFLAGS} ${FFTWFLGS} -c -o $@

clean:
	rm -f *.o
	rm -f *.mod
	rm -f dynamik2
